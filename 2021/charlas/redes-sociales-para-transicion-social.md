---
layout: 2021/post
category: talks
author: Niko
title: Redes sociales para una transición social
---

## {{ page.author }} - {{ page.title }}

Las redes sociales libres y federadas (El Fediverso) tienen un potencial increible que se debe dar a conocer a las personas. Me encantaría poder hablar de eso!

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Si bien el software libre no está adoptado de forma másiva, hay un campo en el que no se usa ni un (5% me atrevería a decir). Y este campo son las redes sociales.

El Fediverso es un mundo de redes sociales federadas que buscan un estandar en este campo, y viene para quedarse como el e-mail. Es solo cuestión de tiempo, de conocimiento y de mejora que esto acabe siendo así.

-   Web del proyecto: <https://www.nogafam.es>

## Público objetivo

Cualquier persona que como minimo sepa que es un ordenador y qué es un móvil, no pediría nada más, la verdad

## Ponente(s)

Niko, el Admin de los servicios de nogafam.es:

Empecé siendo desarrollador de software y terminé en el lado oscuro (ingeniero de sistemas) de una empresa de Bilbao dedicada al Open Source. El artículo de como surgió mi proyecto detalla más cosas de mi recorrido https://www.nogafam.es/como-surge-el-proyecto/.

Como experiencia, he realizado algunos podcasts en colaboración de otra gente del Fediverso y también he formado parte de charlas online sobre la soberania digital.

### Contacto(s)

-   Nombre: Niko
-   Email: admin@nogafam.es
-   Web personal:
-   Mastodon (u otras redes sociales libres): <https://masto.nogafam.es/@admin>
-   Twitter:
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios

Creo que no mucho más que añadir, tengo unas ganas inmensas de promover ideas y enseñar las cosas que he aprendido en el camino de la soberanía digital que tengo

## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) de esLibre y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
