---
layout: 2021/post
category: talks
author: Dario Castañé
title: Libertad para compartir
---

## {{ page.author }} - {{ page.title }}

Charla sobre la Iniciativa Ciudadana Europea Freedom to Share: https://freesharing.eu/es

## Formato de la propuesta

Indicar uno de estos:
-   [x]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

Esta iniciativa aboga por la adopción de una disposición legislativa que contemple una exención de los derechos de autor, derechos afines y derechos sui generis del fabricante de bases de datos para las personas físicas que comparten archivos a través de redes digitales para fines personales y sin ánimo de lucro.

-   Web del proyecto: <https://freesharing.eu/es>

## Público objetivo

General

## Ponente(s)

Dario Castañé

Ingenierio informático, activista político y contribuidor en software libre. Actualmente dedica su (escaso) tiempo libre a proveer de herramientas de voto a organizaciones sin ánimo de lucro, mantener Mergo - una librería que vive en el corazón del cloud, usada por Docker, Kubernetes, etc. - desarrollada en Go, y buscar nuevos campos en los que trabajar por un impacto social positivo usando la tecnología.

### Contacto(s)

-   Nombre: Dario Castañé
-   Email:
-   Web personal: <https://da.rio.hn>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@dario>
-   Twitter: <https://twitter.com/darccio>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/imdario>

## Comentarios



## Preferencias de privacidad

-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
