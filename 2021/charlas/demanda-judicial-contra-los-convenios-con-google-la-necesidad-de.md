---
layout: post
category: talks
author: Luis Fajardo
title: Demanda judicial contra los convenios con Google la necesidad de la acción colectiva.
---

## {{ page.author }} - {{ page.title }}

Al principio de la pandemia reaccioné como padabogado y DPD: el convenio con Google de mi comunidad autónoma es ilegal (como los demás), y serviría además de ariete para abrirles las puertas de otras. Pedí el expediente, lo recurrí ante la Administración, pero para la vía judicial no podía presentarlo solo... Te contaré los argumentos, lo que ha venido después preparando Tecnologías para la Sociedad, y cómo puedes colaborar para presentarla antes del comienzo de curso en toda España.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Hace ya más de un año que nos vimos obligados, llevados por la pandemia, a hacer frente al desembarco masivo de tecnologías que invaden la privacidad de nuestros hogares. Como profesor monté un #Jitsi para dar mis clases, colaboré más que me enfrenté a las políticas de mi universidad, y junto a otros compañeros terminamos montando el embrión de #EDUCATIC, que hoy gestiona la plataforma de videoconferencias libre (BigBlueButton) con la que impartimos docencia, con interés de los compañeros (impartimos un curso a profesorado al que se inscribieron 179) y el beneplácito del Rectorado. Seguimos avanzando porque queda mucho por hacer.
Finalmente constituimos la Asociación EDUCATIC, Tecnologías para la Educación y Transparencia Tecnológica, una de cuyas finalidades es dar vida a una fundación que garantice la continuidad del proyecto: txs.es, correo libre y seguro, redes sociales libres y seguras, herramientas para el trabajo que respeten nuestros valores (y nuestras leyes, claro), y cómo no, aplicaciones para la docencia. Queremos seguir trabajando para que los centros educativos en España utilicen tecnología ética, que enseñen a nuestra juventud el potencial de la tecnología, sin hacerlos usuarios cautivos de plataformas de captación de datos y consumo tecnológico.
Ahí entran las acciones judiciales, que detallaré. Puedes ver más en [esta entrada](en https://encanarias.info/posts/19db429025a40139409b26860af9fe8c) de mi blog en Diaspora.

-   Web del proyecto: <https://www.educar.encanarias.info/fundacion/>

## Público objetivo

Padres, profesores, educadores y cualquier persona preocupada por la entrada de la educación en el mercado del capitalismo salvaje de la vigilancia.

## Ponente(s)

Luis Fajardo López, Prof. de Derecho civil en la ULL. Defensor de las libertades en el ciberespacio. Dr. por la UAM, ha dado clases en la UAM, UAL, UdG, y UNED. Ha sido abogado, Juez, y DPD. Actualmente está centrado en su trabajo docente y de investigación en la ULL y en la constitución de la Fundación Tecnología para la Sociedad, txs.es.

### Contacto(s)

-   Nombre: Luis Fajardo
-   Email:
-   Web personal: <https://encanarias.info/people/56066f30d1620135a1394b44294f2d2e>
-   Mastodon (u otras redes sociales libres): <https://txs.es/@lfajardo>
-   Twitter: <https://twitter.com/lfajardo>
-   GitLab: <https://gitlab.com/luisfa>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://git.txs.es/lfajardo>

## Comentarios



## Preferencias de privacidad

-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) de esLibre y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
